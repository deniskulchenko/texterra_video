$( document ).ready(function() {

	$('.watch__portfolio__button').click(function(o) {
		event.preventDefault(o);
		$('html, body').animate({scrollTop: $('.examples__video__section').offset().top  }, 800);
	});

	$('.overlay').click(function() {
		$('.pop__up__form').fadeOut();
		$(this).fadeOut();
	});

	$('.pop__up__close').click(function() {
		$(this).parents('.pop__up__form').fadeOut();
		$('.overlay').fadeOut();
	});


	$('.section__callback__btn, .order__button').click(function() {
		$('.overlay').fadeIn();
		$('.pop__up__form').fadeIn();
	});

});